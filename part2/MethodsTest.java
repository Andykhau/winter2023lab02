public class MethodsTest
{
  public static void main(String [] args)
  {
	  
	int x = 5;
	System.out.println(x);
	double y = 3;
	methodNoInputNoReturn();
	
	System.out.println(x);
	
	methodOneInputNoReturn(x + 10);
	
    System.out.println(x);
	
	//methodTwoInputNoReturn(y,x);
	
	int z = methodNoInputReturnInt();
	System.out.println("Print z: "+z);
	
	double c = sumSquareRoot(9,5);
	System.out.println("print the squareRoot: "+c);
	
	String s1 = "java";
	String s2 = "programming";
	System.out.println("print s1: "+s1.length());
	
	SecondClass add = new SecondClass();
	
	int plusOne = add.addOne(50);
	System.out.println(plusOne);
	
	int plusTwo = add.addTwo(50);
	System.out.println(plusTwo);
	
	SecondClass sc = new SecondClass();
	int plusTwoPart2 = sc.addTwo(50);
	System.out.println(plusTwoPart2);
  }
  
  public static void methodNoInputNoReturn()
  {
	System.out.println("I'm in a method that takes no input and returns nothing");
	int x = 20;
	System.out.println(x);
  }
  
  public static void methodOneInputNoReturn(int y)
  {
	y = y - 5;
	System.out.println("Inside the method one input no returns: " + y);

  }
  
  public static void methodTwoInputNoReturn(int x, double y)
  {
	System.out.println("print int: "+x);
    System.out.println("print double:"+y);
  }
  public static int methodNoInputReturnInt()
  {
	return 5;
  }
  
  public static double sumSquareRoot(int a,int b)
  {
	return Math.sqrt(a+b);
  }

}