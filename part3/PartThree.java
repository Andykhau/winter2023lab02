import java.util.Scanner;
public class PartThree
{
	public static void main(String[]args)
	{
		Scanner scan = new Scanner(System.in);
		calculator cal = new calculator();
		
		System.out.println("Enter your first number");
		int x = scan.nextInt();
		
		System.out.println("Enter your second number");
		int y = scan.nextInt();
		
		double a = cal.add(x,y);
		System.out.println("Result of addition: "+a);
		
		double b = cal.subtract(x,y);
		System.out.println("Result of subtract: "+b);
		
		double c = cal.multiply(x,y);
		System.out.println("Result of multiply: "+c);
		
		double d = cal.divide(x,y);
		System.out.println("Result of divide: "+d);
	}
}